# Danny's Home Assistant config files

[![HA Version](https://img.shields.io/badge/Running%20Home%20Asssistant-2021.1.7%20-darkblue)](https://github.com/home-assistant/core/releases/tag/2021.1.7)


## Table of content

- [My devices](#my-devices)
- [Hardware/Software backbone](#hardware-software)


## My devices

<!-- start-table -->

<table>
    <thead>
        <tr>
            <th>Switches 🎚</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>4x Wink Relay</td>
        </tr>
        <tr>
            <td>7x Insteon ToggleLinc</td>
        </tr>
        <tr>
            <td>1x Zooz ZEN24</td>
        </tr>
        <tr>
            <td>3x Insteon LampLinc</td>
        </tr>
        <tr>
            <td>1x Inston Outdoor On/Off Module</td>
        </tr>     
        <tr>
            <td>2x Innr Zigbee Plug</td>
        </tr>              
    </tbody>
    <thead>
        <tr>
            <th>Sensors 🌡</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>2x Aqara Temperature/Humidity Sensor</td>
        </tr>
        <tr>
            <td>1x Insteon Motion Sensor</td>
        </tr>
        <tr>
            <td>1x Ecolink Motion Sensor</td>
        </tr>
        <tr>
            <td>Ecolink Z-Wave Tilt Sensor</td>
        </tr>
        <tr>
            <td>8x Generic 433 Door/Window Sensors</td>
        </tr>        
    </tbody>
    <thead>
        <tr>
            <th>Vacuum 🧹</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Ecovacs DeeBot 901</td>
        </tr>
    </tbody>
    <thead>
        <tr>
            <th>Media player 📺🔈</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1x Amazon Fire Tv Stick 4K</td>
        </tr>
        <tr>
            <td>5x Amazon Echo Dot 3rd Gen</td>
        </tr>
        <tr>
            <td>1x Amazon Echo 2nd Gen</td>
        </tr>
        <tr>
            <td>1x Amazon Echo 1st Gen</td>
        </tr>
        <tr>
            <td>1x Amazon Echo Show 5</td>
        </tr>       
        <tr>
            <td>1x Amazon Echo Spot</td>
        </tr>                       
    </tbody>
    <thead>
        <tr>
            <th>Lights 💡</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>3x Philips Hue E27</td>
        </tr>
        <tr>
            <td>3x Ikea Tradfi E27</td>
        </tr>
        <tr>
            <td>4x Lifx 1000</td>
        </tr>
        <tr>
            <td>1x Lifx Z Strip</td>
        </tr>
        <tr>
            <td>5x Sylvania Smart+</td>
        </tr>
        <tr>
            <td>2x Sengled Element</td>
        </tr>
        <tr>
            <td>3x NodeMCU w/ WS2812b (WLED)</td>
        </tr>        
    </tbody>
    <thead>
        <tr>
            <th>Hubs 🌎</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>CC2531 Sniffer</td>
        </tr>
        <tr>
            <td>NORTEK HUSBZB-1</td>
        </tr>    
        <tr>
            <td>Insteon Hub 2245</td>
        </tr>                  
    </tbody>
    <thead>
        <tr>
            <th>Cameras</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>4x Reolink RLC-410</td>
        </tr>
    </tbody>
</table>
<!-- end-table -->


# Hardware-Software

I run a [Supervised install](https://www.home-assistant.io/getting-started/) in a Proxmox LXC Container using [Whiskerz007](https://github.com/whiskerz007/proxmox_hassio_lxc) as my main instace running on a cluster of the following servers: 

<!-- start-table -->
<table>
    <thead>
        <tr>
            <th>Server Name</th>
            <th>CPU</th>
            <th>RAM</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Nebula</td>
            <td>AMD FX-6300</td>
            <td>16GB</td>
        </tr>
        <tr>
            <td>Thanos</td>
            <td>AMD Turion N40L</td>
            <td>8GB</td>
        </tr>
        <tr>
            <td>Gamora</td>
            <td>I7-6700k</td>
            <td>32GB</td>
        </tr>
    </tbody>
</table>
<!-- end-table -->

I also run various Raspberry Pis around the house:

<!-- start-table -->
<table>
    <thead>
        <tr>
            <th>Model</th>
            <th>Purpose</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>3B</td>
            <td>Zigbee/Z-Wave Bridge + Room Assistant</td>
        </tr>
        <tr>
            <td>Zero W</td>
            <td>Room Assistant</td>
        </tr>
        <tr>
            <td>Zero W</td>
            <td>Room Assistant + Webber iGrill MQTT</td>
        </tr>  
        <tr>
            <td>3B</td>
            <td>Room Assistant + Spotify Connect (w/ HiFiBerry Amp2)</td>
        </tr>                     
    </tbody>
</table>
<!-- end-table -->
